<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Productos;
use App\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;
use DB;

class VentaController extends Controller
{
    /*
        ruta: /api/venta/store-venta
        {
            "numero_factura": "25566",
            "cliente": "Julian Marin",
            "telefono": "3225668741",
            "email": "julian.marin@gmail.com",
            "productos": [
                {"sku": "1222", "cantidad": "5"},
                {"sku": "255", "cantidad": "2"}
            ]
        }
    */
    public function storeVenta(Request $request){
        $validator = Validator::make(
            $request->all(),[
                'numero_factura' => 'required',
                'cliente' => 'required|max:500|min:5',
                'telefono' => 'required|max:30|min:5',
                'email' => 'required|email'
            ],
            [
                'numero_factura.required' => 'Número de factura requerido',
                'cliente.required' => 'Cliente requerido',
                'cliente.max' => 'Cliente máximo 250 caracteres',
                'cliente.min' => 'Cliente mínimo 5 caracter',                
                'telefono.required' => 'Telefono requerido',
                'telefono.max' => 'Telefono máximo 30 caracteres',
                'telefono.min' => 'Telefono 5 caracter',
                'email.required' => 'Email requerido',
                'email.email' => 'Email invalido'
            ]
        );

        if ($validator->fails()){
            return response()->json(array('errors' => $validator->messages()), 200);
        }else{
            try{
                if(count($request->productos) > 0){
                    $subtotal = 0;
                    $total = 0;
                    foreach($request->productos as $producto){
                        $obtener_producto = Producto::find($producto["sku"]);
                        if(!$obtener_producto){
                            return response()->json(array('save' => false, 'message' => 'Error, el producto '.$producto["sku"].' no existe, por favor verifique'), 200);
                        }else{
                            $subtotal += $obtener_producto->precio * $producto["cantidad"];
                            $total += (($obtener_producto->precio * $obtener_producto->iva) + $obtener_producto->precio) * $producto["cantidad"];
                        }
                    }
                    DB::beginTransaction();
                    $venta = new Venta();
                    $venta->numero_factura = $request->numero_factura;
                    $venta->cliente = $request->cliente;
                    $venta->telefono = $request->telefono;
                    $venta->email = $request->email;
                    $venta->total = $total;
                    $venta->subtotal = $subtotal;
                    if($venta->save()){
                        foreach($request->productos as $producto){
                            $crer_productos = new Productos();
                            $crer_productos->sku = $producto["sku"];
                            $crer_productos->id_venta = $venta->id_venta;
                            $crer_productos->cantidad = $producto["cantidad"];
                            $crer_productos->save();
                        }
                        DB::commit();
                        return response()->json(array('save' => true, 'message' => 'Venta generada exitosamente con numero de factura: '.$request->numero_factura), 200);
                    }else{
                        return response()->json(array('save' => false, 'message' => 'Error al generar la venta. Porfavor contáctese con el administrador'), 200);
                    }
                }
            }catch(\Exception $e){
                Log::error($e);
                DB::rollback();    
                return response()->json(array('success' => false), 500);
            }
        }
    }
}
