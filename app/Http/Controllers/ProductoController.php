<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;
use DB;

class ProductoController extends Controller
{
    //
    public function index(){
        try{
            DB::beginTransaction();
            $productos = Producto::get();
            if(count($productos) > 0){
                DB::commit();
                return response()->json(array('get' => true, 'data' => $productos), 200);
            }else{
                return response()->json(array('get' => false, 'message' => 'No hay productos que listar'), 200);
            }
        }catch(\Exception $e){
            Log::error($e);
            DB::rollback();    
            return response()->json(array('success' => false), 500);
        }
    }

    /*
        {
            "sku": "1222",
            "nombre": "Avena liquida",
            "descripcion": "300 miligramos",
            "foto": "avena.jpg",
            "precio": "5500",
            "iva": "0.19",
        }
    */
    public function store(Request $request){
        $validator = Validator::make(
            $request->all(),[
                'sku' => 'required|max:25',
                'nombre' => 'required|max:250|min:5',
                'descripcion' => 'required|max:500|min:5',
                'precio' => 'required',
                'foto' => 'required',
                'iva' => 'required',
            ],
            [
                'sku.required' => 'SKU del producto requerido',
                'sku.max' => 'SKU del producto máximo 25 caracteres',
                'nombre.required' => 'Nombre del producto requerido',
                'nombre.max' => 'Nombre del producto máximo 250 caracteres',
                'nombre.min' => 'Nombre del producto mínimo 5 caracter',                
                'descripcion.required' => 'Descripción del producto requerido',
                'descripcion.max' => 'Descripción del producto máximo 500 caracteres',
                'descripcion.min' => 'Descripción del producto mínimo 5 caracter',
                'precio.required' => 'Precio requerido',
                'iva.required' => 'Iva requerido',
                'foto.required' => 'Foto requerida'
            ]
        );

        if ($validator->fails()){
            return response()->json(array('errors' => $validator->messages()), 200);
        }else{
            try{
                DB::beginTransaction();
                $producto = new Producto();
                $producto->sku = $request->sku;
                $producto->nombre = $request->nombre;
                $producto->descripcion = $request->descripcion;
                $producto->precio = $request->precio;
                $producto->iva = $request->iva;
                $producto->foto = $request->foto;

                if($producto->save()){
                    DB::commit();
                    return response()->json(array('save' => true, 'message' => 'Producto almacenado exitosamente'), 200);
                }else{
                    return response()->json(array('save' => false, 'message' => 'Error al guardar el producto. Porfavor contáctese con el administrador'), 200);
                }
            }catch(\Exception $e){
                Log::error($e);
                DB::rollback();            
                return response()->json(array('success' => false), 500);
            } 
        }
    }

    public function update(Request $request){
        $validator = Validator::make(
            $request->all(),[
                'sku' => 'required|max:25'
            ],
            [
                'sku.required' => 'SKU del producto requerido',
                'sku.max' => 'SKU del producto máximo 25 caracteres'
            ]
        );

        if ($validator->fails()){
            return response()->json(array('errors' => $validator->messages()), 200);
        }else{
            try{
                DB::beginTransaction();
                $producto = Producto::find($request->sku);
                $producto->nombre = $request->nombre;
                $producto->descripcion = $request->descripcion;
                $producto->precio = $request->precio;
                $producto->iva = $request->iva;
                $producto->foto = $request->foto;

                if($producto->save()){
                    DB::commit();
                    return response()->json(array('update' => true, 'message' => 'Producto actualizado exitosamente'), 200);
                }else{
                    return response()->json(array('update' => false, 'message' => 'Error al actualizar el producto. Porfavor contáctese con el administrador'), 200);
                }
            }catch(\Exception $e){
                Log::error($e);
                DB::rollback();            
                return response()->json(array('success' => false), 500);
            } 
        }
    }

    public function show(Request $request){
        $validator = Validator::make(
            $request->all(),[
                'sku' => 'required|max:25'
            ],
            [
                'sku.required' => 'SKU del producto requerido',
                'sku.max' => 'SKU del producto máximo 25 caracteres',
            ]
        );

        if ($validator->fails()){
            return response()->json(array('errors' => $validator->messages()), 200);
        }else{
            try{
                DB::beginTransaction();
                $producto = Producto::find($request->sku);
                if($producto){
                    DB::commit();
                    return response()->json(array('get' => true, 'data' => $producto), 200);
                }else{
                    return response()->json(array('get' => false, 'message' => 'El producto no existe. Por favor verifique'), 200);
                }
            }catch(\Exception $e){
                Log::error($e);
                DB::rollback();    
                return response()->json(array('success' => false), 500);
            }
        }
    }

    public function destroy(Request $request){
            $validator = Validator::make(
            $request->all(),[
                'sku' => 'required|max:25'
            ],
            [
                'sku.required' => 'SKU del producto requerido',
                'sku.max' => 'SKU del producto máximo 25 caracteres',
            ]
        );

        if ($validator->fails()){
            return response()->json(array('errors' => $validator->messages()), 200);
        }else{
            try{
                DB::beginTransaction();
                if(Producto::destroy($request->sku)){
                    DB::commit();
                    return response()->json(array('delete' => true, 'message' => 'Producto eliminado exitosamente'), 200);
                }else{
                    return response()->json(array('delete' => false, 'message' => 'El producto no se puede eliminar. Por favor verifique'), 200);
                }
            }catch(\Exception $e){
                Log::error($e);
                DB::rollback();    
                return response()->json(array('success' => false), 500);
            }
        }
    }
}
